var popupAfterDone = false,
    sheetName = 'PF1.LootSheetPf1NPC',
    userPerm = {default: 0},
    allTokens = canvas.tokens.controlled;
    
game.users.entries.forEach(o => {
    if (o.character) userPerm[o.id] = 2;
});
var updateTok = async function(singleToken) {
    await singleToken.actor.sheet.close();
    if (singleToken.actor.getFlag('core', 'sheetClass') === 'PF1.NPC' || singleToken.actor.getFlag('core', 'sheetClass') === 'PF1.NPC (Lite)' || !singleToken.actor.getFlag('core', 'sheetClass')) {
        await singleToken.actor.setFlag('core', 'sheetClass', sheetName)
    }
    await singleToken.actor.update({'permission': userPerm});
    if (popupAfterDone) await singleToken.actor.sheet.render(true);
};
allTokens.forEach((tok) => updateTok(tok));