// CONFIGURATION
// actorNames overrides selected tokens.
// Leave empty to use currently selected tokens/ fill to always use the same actorNames
// Example actorNames: `actorNames: ["Bob", "John"],`
// The rest are localization strings.
// Conditions should apply to the current localization but these config strings need to be done manually.
const c = {
	actorNames: [],
	windowTitle: "Condition Selection",
	windowSubtitle: "Apply conditions to the following tokens: ",
	buttonLabels: ["Add", "Remove", "Apply", "Clear"],
	warningMissingActors: "No applicable actor(s) found"
};
// END CONFIGURATION

//Set up targets and verify they can be edited
const myTokens = canvas.tokens.controlled;
let myActors;
if (c.actorNames.length > 0)
	myActors = game.actors.filter(o => c.actorNames.includes(o.name));
else
	myActors = myTokens.map(o => o.actor);
myActors = myActors.filter(o => o.hasPerm(game.user, "OWNER"));

//Main codeblock
if (!myActors.length) ui.notifications.warn(c.warningMissingActors);
else {
	
	let _addCond = function(htm) {
		let checks = Array.from(htm.querySelectorAll('input:checked')).map(o => [o.name, true]);
		myActors.forEach(o => {
			o.update(Object.fromEntries(checks));
		});
	}
	
	let _remCond = function(htm) {
		let checks = Array.from(htm.querySelectorAll('input:checked')).map(o => [o.name, false]);
		myActors.forEach(o => {
			o.update(Object.fromEntries(checks));
		});
	}
	
	let _applyCond = function(htm) {
		let checks = Array.from(htm.querySelectorAll('input')).map(o => [o.name, o.checked]);
		myActors.forEach(o => {
			o.update(Object.fromEntries(checks));
		});
	}
	
	let _clearCond = function(htm) {
		let checks = Array.from(htm.querySelectorAll('input')).map(o => [o.name, false]);
		myActors.forEach(o => {
			o.update(Object.fromEntries(checks));
		});
	}
	
	var msg = '<p>' + c.windowSubtitle + myActors.map(o => o.name).join(', ') + '</p><div class="flexrow">';
	let	conditions = Object.keys(CONFIG.PF1.conditions);
		
	conditions.forEach(o => {
		msg += `
<div style="flex: auto; padding: 0 .5em;">
	<label class="checkbox">
	<input type="checkbox" name="data.attributes.conditions.${o}">
	<span>${game.i18n.localize("PF1." + "Cond" + o.charAt(0).toUpperCase() + o.slice(1))}</span>
	<label>
</div>`;
	});
	msg += '</div>';
	
	const dialog = new Dialog({
		title: game.i18n.localize('PF1.ConditionPlural'),
		content: `<p>${msg}</p>`,
		buttons: {
			add: {
				label: c.buttonLabels[0],
				callback: html => {
					_addCond(html[0])
				}
				
			},
			remove: {
				label: c.buttonLabels[1],
				callback: html => {
					_remCond(html[0])
				}
			},
			apply: {
				label: c.buttonLabels[2],
				callback: html => {
					_applyCond(html[0]);
				}
			},
			clear: {
				label: c.buttonLabels[3],
				callback: html => {
					_clearCond(html[0]);
				}
			}
		}
	});
	
	dialog.activateListeners = function(html) {
		Dialog.prototype.activateListeners.call(this, html);

		[...html.find("label.checkbox input")].forEach(inp => {
			var affected = myActors.filter(act => getProperty(act.data, inp.name));
			if (affected.length > 0) {
				inp.indeterminate = true;
				inp.parentNode.title = affected.map(act => act.name);
			}
		});
	};
	
	dialog.render(true);
	
}