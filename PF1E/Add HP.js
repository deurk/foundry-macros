//Add or subtract HP. Will do math or rolls. Clamped to min and max
//Preface with "=" to set to exactly that number
/* Alt Use: */
//Can be used wtihin entity link buttons. Anything but the first number is ignored.
//"->" can be used to subtract the number of the next inline roll encountered
//"+>" can be used to add the number of the next inline roll encountered
//"=>" can be used to set to the number of the next inline roll encountered
//Examples: (All are equivalent)
//	@Macro[Add HP]{Add HP ->}[[5+3]]
//	@Macro[Add HP]{+8 HP}
//	@Macro[Add HP]{Set to =>}[[@attributes.hp.value + 3 + 5]]

var controlled = canvas.tokens.controlled.map(o => o.actor),
	inputText = false,
	chatMessage = game.messages.get(event.srcElement.closest('.message')?.getAttribute('data-message-id'));
if (window.macroChain?.length || event.srcElement.nodeName == 'A')
	inputText = window.macroChain?.pop() || event.srcElement.textContent.trim();

if (inputText) {
	if (inputText.indexOf('->') > -1)
		inputText = '-1 * (' + event.srcElement.nextElementSibling?.textContent.trim() + ')';
	else if (inputText.indexOf('+>') > -1)
		inputText = event.srcElement.nextElementSibling?.textContent.trim();
	else if (inputText.indexOf('=>') > -1)
		inputText = '=' + event.srcElement.nextElementSibling?.textContent.trim();
	else
		inputText = inputText.match(/([=-]?\d)+/)?.[1];
}
if (!inputText) {
	let d = new Dialog({
		title: 'How much HP?',
		content: '<input name="temphp" type="text" class="hp select-on-click" placeholder="+HP." title="Hit Points" autofocus>',
		buttons: {
		  ok: {
			icon: '',
			label: "OK",
			callback: applyHP
		  }
		},
		default: 'ok'
	  }, {width: 100});
	Hooks.once('renderDialog', (a,inp) => inp[0].querySelector('input').focus());
	d.render(true);
}
else
	applyHP(null, inputText);

function applyHP(htm, fromButton=false) {
	controlled.forEach(selected => {
		let change = 0, equal;
		if (fromButton || fromButton === '0')
			change = fromButton;
		else
			change = htm[0].querySelector('input').value;
		if (change.charAt(0) == '=') {
			equal = true;
			change = change.slice(1);
		}
		let parsed = parseInt((new Roll(change).roll()).total),
			difference = Math.max(selected.data.data.attributes.hp.value + parsed, 0);
		difference = Math.clamped(difference, selected.data.data.attributes.hp.min, selected.data.data.attributes.hp.max);
		parsed = Math.clamped(parsed, selected.data.data.attributes.hp.min, selected.data.data.attributes.hp.max);
		if (equal)
			selected.update({'data.attributes.hp.value': parsed});
		else
			selected.update({'data.attributes.hp.value': difference});
	});
}