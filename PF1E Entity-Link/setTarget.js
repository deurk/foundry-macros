/*-			DOCUMENTATION			-*/
// Syntax: [Operator] <Type> <Targets>
// [Set/Add/Remove/Clear] selected,target,Character Names
// ie Target template   /  Select myself  /  Target template,-me / clear Selection
// Type:
//		Target(s):			use the targetting system
//		Select(ion):		use the selection system
// Targets (Comma separated, "-" in front negates):
//		selected: 			Default if argument is provided. User who clicks the button
//		target(s/ed):		Targets of user who clicks the button
// 		Character Name:		All tokens on current scene linked to this actor. No commas in the name. Everything else is okay
//		template:			All character tokens within the last template of the user who clicks the button
//		me/myself:			Character the card containing the button came from
//		friendly/ies:		Tokens with disposition "friendly"
//		neutral(s):			Tokens with disposition "neutral" (GM only unless subtracting)
//		hostile(s):			Tokens with disposition "hostile" (GM only unless subtracting)
//		PC(s):				PC tokens
//		NPC(s):				NPC tokens (GM only unless subtracting)
// Operator
//		Set:				Default. overwrites all tokens to specified user
//		Add:				Adds the tokens to user instead
//		Remove:				Removes the tokens from user instead
//		Clear:				Sets type to blank. Ignores <Targets> If "Control" is held down during use, will overwrite operator to Clear
// When clicked from macrohotbar or not chat will set the current targets to Macro title using above syntax
// Supports label mode. [["Target friendlies,-myself,-Lucky the Raven",1]]@Macro[setTarget]{::Target everyone else}

/*-			COMMAND					-*/
try {
	var inputText = window.macroChain?.pop() || event.srcElement.closest('button,a')?.textContent.trim(),
		chatMessage = game.messages.get(event.srcElement.closest('.message')?.getAttribute('data-message-id')),
		argParse = /^(?:([sS]et|[aA]dd|[rR]emove|[cC]lear) )?([tT]argets?|[sS]elect(?:ion)?)(?: (.*))?$/,
		operator, systemType, targets,
		myChar = game.actors.get(chatMessage?.data.speaker?.actor) ?? game.user.character ?? canvas.tokens.controlled[0]?.actor,
		myself = game.user,
		targetActors = [],
		excludedActors = [],
		tokenIds = [];
	if (inputText.indexOf('::') == 0)
		inputText = /['`"](.*)['`"],.*/.exec(event.srcElement.previousElementSibling?.title.trim())?.[1];
	[operator, systemType, targets] = inputText.match(argParse).slice(1);
	
	if (!operator) operator = 'set';
	else operator = operator.toLowerCase();
	if (game.keyboard.isDown("Control"))
		operator = 'clear';
	
	targets?.split(',').forEach(tar => {
		var accuActors;
		tar = tar.trim();
		if (tar.charAt(0) == '-') {
			accuActors = excludedActors;
			tar = tar.slice(1);
		}
		else
			accuActors = targetActors;
		switch(tar.toLowerCase()) {
			case 'selected':
				if (canvas.tokens.controlled.length == 0) ui.notifications.warn('No tokens selected');
				accuActors.push(...canvas.tokens.controlled);
				break;
			case 'template':
				let temp = chatMessage?.data.flags.pf1?.metadata?.template ?? canvas.templates.objects.children.filter(o => o.data.user == game.userId)?.pop()?.id;
				if (!temp) ui.notifications.warn('No template found');
				else {
					let tokens = canvas.tokens.objects.children.filter(o => canvas.grid.getHighlightLayer('Template.' + temp).geometry.containsPoint(o.center));
					accuActors.push(...tokens);
				}
				break;
			case 'target':
			case 'targets':
			case 'targeted':
				accuActors.push(...[...game.user.targets]);
				break;
			case 'myself':
			case 'me':
				accuActors.push(...canvas.tokens.objects.children.filter(o => o.actor.id == myself.id));
				break;
			case 'friendly':
			case 'friendlies':
				accuActors.push(...canvas.tokens.objects.children.filter(o => o.data.disposition == 1));
				break;
			case 'neutral':
			case 'neutrals':
				if (myself.isGM || accuActors === excludedActors)
					accuActors.push(...canvas.tokens.objects.children.filter(o => o.data.disposition == 0));
				break;
			case 'hostile':
			case 'hostiles':
				if (myself.isGM || accuActors === excludedActors)
					accuActors.push(...canvas.tokens.objects.children.filter(o => o.data.disposition == -1));
				break;
			case 'pcs':
			case 'pc':
				accuActors.push(...canvas.tokens.objects.children.filter(o => o.actor.data.type == 'character'));
				break;
			case 'npcs':
			case 'npcs':
				if (myself.isGM || accuActors === excludedActors)
					accuActors.push(...canvas.tokens.objects.children.filter(o => o.actor.data.type == 'npc'));
				break;
			default:
				//Name or uuid search
				if (tar.indexOf('#') != 0) {
					let actName = game.actors.find(o => o.name == tar).name;
					accuActors.push(...canvas.tokens.objects.children.filter(o => o.actor.name == actName));
				}
				else {
					var [uScene, uToken, uActor] = tar.match(/^(?:Scene\.([^.]*))?(?:\.Token\.([^.]*))?(?:\.?Actor\.([^.]*))?$/).slice(1);
					if (uActor)
						accuActors.push(...canvas.tokens.objects.children.filter(o => o.actor.id == uActor));
					else if (uScene == canvas.id)
						accuActors.push(canvas.tokens.objects.children.find(o => o.id == uToken));
				}
		}
	});
	//actors here actually refers to tokens because uuid doesn't have support for unlinked tokens
	excludedActors = excludedActors.map(o => o.id);
	targetActors = [...new Set(targetActors)].filter(o => o && !excludedActors.includes(o.id));
	excludedActors = [];
	
	//Written like this so it can bloat easier with the inevitable feature creep
	switch(systemType.toLowerCase()) {
		case 'target':
		case 'targets':
			systemType = 'target';
			tokenIds = targetActors.map(o => o.id);
			break;
		case 'select':
		case 'selection':
			systemType = 'select';
			targetActors = targetActors.filter(o => o.owner);
			break;
		default:
			throw 'Wrong Type. Use Select or Target';
	}
	
	switch(operator.toLowerCase()) {
		case 'set':
			if (systemType == 'target')
				myself.updateTokenTargets(tokenIds);
			else {
				canvas.activeLayer.releaseAll();
				targetActors.forEach(o => {if (o.owner) o.control({releaseOthers: false})});
			}
			break;
		case 'add':
			if (systemType == 'target')
				myself.updateTokenTargets(myself.targets.ids.concat(tokenIds));
			else
				targetActors.forEach(o => {if (o.owner) o.control({releaseOthers: false})});
			break;
		case 'remove':
			if (systemType == 'target')
				myself.updateTokenTargets(myself.targets.ids.filter(p => !tokenIds.includes(p)));
			else
				targetActors.forEach(o => {o.release()});
			break;
		case 'clear':
			if (systemType == 'target')
				myself.updateTokenTargets([]);
			else
				canvas.activeLayer.releaseAll();
			break;
	}
}
catch (err) {
	console.log(err, "Whatever you did didn't work");
}