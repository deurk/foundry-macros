/*-			DOCUMENTATION			-*/
// Syntax: Make/Break [Override]
// Override(s): (comma delimited)
//		Class Name:		the name of a class the spellbook should be render.
//		Group Name:		the spell group the spellbook should use. [primary, secondary, tertiary, spelllike]
// Currently this mostly serves as a helper function for managing spell lists
// To use: Create an item with a description
// @Macro[updateBook]{Make}
// Spell Name 1
// Spell Name 2 ...(etc)
// And then save and click the button created.

/*-			CONFIGURATION			-*/
const c = {
	compendiumPriority: ["world.myspells", "pf1.spells"],
	commands: {
		make: 'Make',
		unmake: 'Break',
		update: 'Update',
		learn: 'Learn',
		sort: 'Sort'
	},
	levels: ['Cantrips/ Orisons', '1st Level', '2nd Level', '3rd Level',
		'4th Level', '5th Level', '6th Level', '7th Level', '8th Level', 
		'9th Level', '10th Level', 'Not Able']
}

/*-			COMMAND					-*/
try {
	var inputText = window.macroChain?.pop() || event.srcElement.closest('button,a')?.textContent.trim(),
		macroId = this.id,
		htmlItem = event.srcElement.closest('.item'),
		packList = [], packPromises = [],
		lclz = Object.keys(c.commands).reduce((res, key) => {res[key] = c.commands[key].toLowerCase(); return res;}, {});

	if (htmlItem) {
		var actId, itId, act, it;
		if (htmlItem.nodeName == 'DIV')
			[actId, itId] = htmlItem.id.match(/actor-(.*)-item-(.*)/).slice(1);
		else if (htmlItem.nodeName == 'LI')
			[actId, itId] = [htmlItem.closest('.actor')?.id.match(/actor-(.*)/)?.[1], htmlItem.getAttribute('data-item-id')];
		act = game.actors.get(actId);
		it = act?.items.get(itId);
		if (!it && !act.data.token.actorLink) {
			act = canvas.tokens.objects.children.filter(o => o.actor.id == act.id).find(i => {
				let found = i.actor?.items.get(itId);
				if (found)
					it = found;
				return found
			}).actor;
		}
	}
	
	if (it && inputText)
		processCommand(inputText, it, act);
	else
		ui.notifications.error(`You can\'t use ${inputText} here.`);
}
catch(err) {
	console.error(err, "Whatever you did didn't work");
}

async function processCommand(command, item, actor) {
	var command_lc = command.toLowerCase(),
		spellList = [],
		rider = '';
	if (command_lc.indexOf(lclz.make) > -1) {
		rider = command.slice(command_lc.indexOf(lclz.make) + lclz.make.length);
		c.compendiumPriority.forEach(c => {
			let pack = game.packs.get(c);
			if (pack) {
				packList.push(pack);
				packPromises.push(pack.getIndex());
			}
		});
		return Promise.all(packPromises).then(comps => makeBook(item, actor, comps, rider));
	}
	if (command_lc.indexOf(lclz.unmake) > -1) {
		rider = command.slice(command_lc.indexOf(lclz.unmake) + lclz.unmake.length);
		unMakeBook(item, actor, rider);
	}
}

function unMakeBook(item, actor, extras) {
	var oldDesc = item.data.data.description.value,
		spellNames = [...oldDesc.matchAll(/@Compendium\[.*?\]{(.*?)}/g)].map(o => o[1]),
		newDesc = `<p>@Macro[updateBook]{${c.commands.make + extras}}</p>\n<p>`;
	spellNames = [...new Set(spellNames)].sort((a,b) => a.localeCompare(b));
	newDesc += spellNames.join('<br />\n') + '</p>';
	item.update({'data.description.value': newDesc});
}

function makeBook(item, actor, packs, extras) {
	var spellPromises = [],
		spellList = (new DOMParser()).parseFromString(item.data.data.description.value.replace(/<br ?\/?>/g, '\n'), 'text/html').body.innerText.split(/[\r\n]+/).slice(1),
		classOverrides = extras.trim().split(/\s*,\s*/g).map(o => o.toLowerCase()),
		brokenLinks = '';

	packs.forEach((pack, pIndex) => {
		pack = sortPack(pack);
		//pack.sort((a,b) => a.name.localeCompare(b.name, undefined, {ignorePunctuation: true}));
		let spellListIndex = 0,
			foundIndex = 0;
		while (spellListIndex < spellList.length) {
			foundIndex = 0;
			foundIndex = binarySearch(pack, spellList[spellListIndex], (sp, it) => sp.localeCompare(it.name, undefined, {ignorePunctuation: true}));
			if (foundIndex >= 0) {
				spellPromises.push(packList[pIndex].getEntity(pack[foundIndex]._id));
				spellList.splice(spellListIndex,1);
			}
			else
				spellListIndex++;
		}
	});
	if (spellList.length > 0) {
		ui.notifications.error('Couldn\'t find ' + spellList.join(', '));
		brokenLinks = spellList.map(s => '@Compendium[broke]{' + s + '}').join('<br />\n');
	}
	Promise.all(spellPromises).then(spells => {
		var newDesc = '',
			type = ['primary', 'secondary', 'tertiary', 'spelllike'],
			newType = [],
			groups, groupKeys;

		if (classOverrides[0] !== '') {
			classOverrides = classOverrides.map(over => {
				let pos = type.indexOf(over);
				if (pos < 0) return over;
				return actor.data.data.attributes.spells.spellbooks[type[pos]].class;
			});
			newType = classOverrides.filter(o => !['','_hd'].includes(o));
		}
		else
			newType = type.filter(t => !['','_hd'].includes(actor.data.data.attributes.spells.spellbooks[t].class)).map(o => actor.data.data.attributes.spells.spellbooks[o].class);
		
		groups = spells.reduce((obj,spell) => {
			var spellClasses = Object.fromEntries(spell.data.data.learnedAt.class),
				sorted = false;
			
			if (spellClasses.hasOwnProperty('sorcerer/wizard')) {
				spellClasses.sorcerer = spellClasses['sorcerer/wizard'];
				spellClasses.wizard = spellClasses['sorcerer/wizard'];
				delete spellClasses['sorcerer/wizard'];
			}
			if (spellClasses.hasOwnProperty('cleric/oracle')) {
				spellClasses.cleric = spellClasses['cleric/oracle'];
				spellClasses.oracle = spellClasses['cleric/oracle'];
				delete spellClasses['cleric/oracle'];
			}
			if (spellClasses.hasOwnProperty('unchained Summoner')) {
				spellClasses.summonerUnchained = spellClasses['unchained Summoner'];
				delete spellClasses['unchained Summoner'];
			}
			newType.forEach(t => {
				let lev = spellClasses[t.toLowerCase()];
				if (lev != undefined) {
					sorted = true;
					if (!obj.hasOwnProperty(t)) obj[t] = {};
					if (!obj[t].hasOwnProperty(lev)) obj[t][lev] = [];
					obj[t][lev].push(spell);
				}
			});
			if (!sorted) {
				if (!obj.hasOwnProperty('none')) obj['none'] = {'11': []};
				obj.none['11'].push(spell);
			}
			return obj;
		}, {});
		
		groupKeys = Object.keys(groups);
		groupKeys.forEach(g => {
			if (groupKeys.length > 1) {
				let catName = g;
				catName = catName.charAt(0).toUpperCase() + catName.slice(1);
				newDesc += `<h1>${catName}</h1>`;
			}
			let levels = Object.keys(groups[g]);
			levels.forEach(l => {
				newDesc += `<h2>${c.levels[l]}</h2><p style="display: flex; flex-wrap: wrap;">`;
				groups[g][l].sort((a, b) => a.name.localeCompare(b.name));
				newDesc += groups[g][l].map(sp => {
					return '@Compendium[' + sp.compendium.metadata.package + '.' + 
						sp.compendium.metadata.name + '.' + sp.id + ']{' + sp.name + '}';
				}).join(' ');
				newDesc += '</p>';
			});
		});
		newDesc += `<section class="secret"><p>@Macro[updateBook]{${c.commands.unmake + extras}}</p>${brokenLinks}</section>`;
		item.update({'data.description.value': newDesc});
	});
}

//Ar is array to be searched, el is element being looked for, compare_fn is comparison
//Returns index or insertion point as negative
function binarySearch(ar, el, compare_fn) {
	var m = 0;
	var n = ar.length - 1;
	while (m <= n) {
		var k = (n + m) >> 1;
		var cmp = compare_fn(el, ar[k]);
		if (cmp > 0) {
			m = k + 1;
		} else if(cmp < 0) {
			n = k - 1;
		} else {
			return k;
		}
	}
	return -m - 1;
}

function sortPack(inputArr) {
	let n = inputArr.length;
		for (let i = 1; i < n; i++) {
			let current = inputArr[i];
			let j = i-1;
			let currentLower = current.name.toLowerCase();
			while ((j > -1) && (currentLower.localeCompare(inputArr[j].name.toLowerCase(), undefined, {ignorePunctuation: true}) < 0)) {
				inputArr[j+1] = inputArr[j];
				j--;
			}
			inputArr[j+1] = current;
		}
	return inputArr;
}

function escapeRegExp(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
}