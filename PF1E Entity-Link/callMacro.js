/*-			DOCUMENTATION			-*/
//Duplicate and change this macro name to the button title
//set targetMacro below to target ie targetMacro = "applyBuff"

/*-			CONFIGURATION			-*/
const targetMacro = "";

/*-			COMMAND					-*/
window.macroChain = window.macroChain?.concat([this.name]) ?? [this.name];
game.macros.find(o => o.name == targetMacro)?.execute();

