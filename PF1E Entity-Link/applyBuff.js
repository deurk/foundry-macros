/*-			DOCUMENTATION			-*/
// Syntax: <Operator> <Buff Name> [to/from/on <targets>] [as <Alt Name>] [at <Level>] [for <Length>]
// Apply/Remove/Toggle/Delete/Activate Buff Name to/from/on selected,target,Character Names at Level
// ie @Macro[applyBuff]{Apply Mage Armor to selected,-myself as Lightest Armor at @cl-1}
// Operator: 
//		Apply:			Create and Activate Buff
//		Remove: 		Delete and Deactivate Buff
//		Toggle: 		Applies if off, deactivates if on
//		Activate:		Turn on buff but only if present
//		Deactivate:		Create and turn off buff if it doesn't exist
//		Change:			Does nothing but update level
// Buff Name:
//		Will use the first Buff from the compendium. If compendiumPriority is specified below, will check those compendiums first, then pf1.commonbuffs.
//		Doesn't short circuit so every compendium provided might be checked
// Targets (Comma separated, "-" in front negates, same rules as Alt Name [see below]):
//		selected: 		Default if argument is provided. User who clicks the button
// 		Character Name:	No commas in the name. Everything else is okay. *Only updates linked actors (by design - convince me otherwise)*
//		target(s)(ed):	All targets of the user who clicks the button
//		template:		All character tokens within the last template of the user who clicks the button
//		me/myself:		Character the card containing the button came from
//		speaker:		Active speaker of the person who clicks the card
//		uuid:			Generally used internally for the time being. Prefix uuids with '#'
// Alt Name:
//		Will search for both alt name and original. Quotes won't work here so use underscores(_) as spaces if you need to use any of the keywords (to,from,on,as,at)
// Level
//		number			Sets item level if supplied.
//      @cl-[1,2,3,s]	Accepts rollData strings. @cl-1, @cl-2, @cl-3, @cl-s are shorthands for @attributes.spells.spellbooks.X.cl.total
//		->				Will use the next inline roll results instead. Useful for pretty lines.
// Supports label mode. [["Apply Mage Armor to selected,-myself as Lightest Armor at @cl-1",1]]@Macro[applyBuff]{::Activate Sparkle Barrier}

/*-			CONFIGURATION			-*/
const c = {
	compendiumPriority: ["world.mybuffs"]
}

/*-			COMMAND					-*/
try {
	var inputText = window.macroChain?.pop() || event.srcElement.closest('button,a')?.textContent.trim(),
		macroId = this.id,
		chatMessage = game.messages.get(event.srcElement.closest('.message')?.getAttribute('data-message-id')),
		argParse = /^([aA]pply|[rR]emove|[tT]oggle|[aA]ctivate|[dD]eactivate|[cC]hange) "?([^\n]*?)"?( (?:to|from|on|as|at|for)(?=([^"\\]*(\\.|"([^"\\]*\\.)*[^"\\]*"))*[^"]*$) (?:.*))?$/,
		operator, buffName, modString,
		compCollNames = c.compendiumPriority.concat(['pf1.commonbuffs']),
		compSearches = [],
		myself = game.actors.get(chatMessage?.data.speaker?.actor) ?? game.user.character ?? canvas.tokens.controlled[0]?.actor,
		targets,
		modList,
		targetActors = [],
		excludedActors = [],
		altName,
		levelOverride;
	
	if (inputText.indexOf('::') == 0)
		inputText = /['`"](.*)['`"],.*/.exec(event.srcElement.previousElementSibling?.title.trim())?.[1];
	[operator, buffName, modString] = inputText.match(argParse).filter(o => o).slice(1,4);
	if (modString) {
		modList = modString.split(/ (to|from|on|as|at) /).slice(1);
		for (var k = 0; k < modList.length; k += 2) {
			switch(modList[k]) {
				case 'to':
				case 'from':
				case 'on':
					targets = modList[k+1].replace(/_/g,' ');
					break;
				case 'as':
					altName = modList[k+1].replace(/_/g,' ');
					break;
				case 'at':
					if (modList[k+1].indexOf('->') > -1)
						modList[k+1] = event.srcElement.nextElementSibling?.textContent.trim();
					levelOverride = modList[k+1];
					if (levelOverride.search(/[^\d]/) > -1) {
						let shorthand = {	'cl-1': myself.data.data.attributes.spells.spellbooks.primary.cl.total,
											'cl-2': myself.data.data.attributes.spells.spellbooks.secondary.cl.total,
											'cl-3': myself.data.data.attributes.spells.spellbooks.tertiary.cl.total,
											'cl-s': myself.data.data.attributes.spells.spellbooks.spelllike.cl.total};
						levelOverride = (new Roll(levelOverride, Object.assign(shorthand, myself.data.data)).roll()).total;
					}
					break;
			}
		}
	}
	if (!targets) targets = 'selected';
	
	(targets ? targets.split(',') : ['selected']).forEach(tar => {
		var accuActors;
		tar = tar.trim();
		if (tar.charAt(0) == '-') {
			accuActors = excludedActors;
			tar = tar.slice(1);
		}
		else
			accuActors = targetActors;
		switch(tar.toLowerCase()) {
			case 'selected':
				if (canvas.tokens.controlled.length == 0) ui.notifications.warn('No tokens selected');
				accuActors.push(...canvas.tokens.controlled.map(o => o.actor));
				break;
			case 'template':
				let temp = chatMessage?.data.flags.pf1?.metadata?.template ?? canvas.templates.objects.children.filter(o => o.data.user == game.userId)?.pop()?.id;
				if (!temp) ui.notifications.warn('No template found');
				else {
					let tokens = canvas.tokens.objects.children.filter(o => canvas.grid.getHighlightLayer('Template.' + temp).geometry.containsPoint(o.center));
					accuActors.push(...tokens.map(o => o.actor));
				}
				break;
			case 'target':
			case 'targets':
			case 'targeted':
				accuActors.push(...[...game.user.targets].map(o => o.actor));
				break;
			case 'myself':
			case 'me':
				accuActors.push(myself);
				break;
			case 'speaker':
				accuActors.push(game.pf1.ActorPF.getActiveActor());
				break;
			case 'remaining':
				tar = '#' + chatMessage?.data.flags.applyBuff?.remaining;
				if (tar == '#undefined')
					break;
			default:
				//Name or uuid search
				if (tar.indexOf('#') != 0)
					accuActors.push(game.actors.find(o => o.name == tar));
				else {
					var [uScene, uToken, uActor] = tar.match(/^#(?:Scene\.([^.]*))?(?:\.Token\.([^.]*))?(?:\.?Actor\.([^.]*))?$/).slice(1);
					if (uActor)
						accuActors.push(game.actors.get(uActor));
					else if (uScene == canvas.id)
						accuActors.push(canvas.tokens.objects.children.find(o => o.id == uToken).actor);
				}
		}
	});
	excludedActors = excludedActors.map(o => o.id);
	targetActors = [...new Set(targetActors)].filter(o => o && !excludedActors.includes(o.id));
	excludedActors = [];
	
	//This is only moved down here so I can assume all the serialized stuff above is done
	compCollNames.forEach((coll, place) => {
		compSearches[place] = new Promise((resolve, reject) => {
			(async () => {
				let pack = game.packs.get(coll);
				if (pack) {
					let search = await pack.getIndex().then(p => p.find(o => o.name == buffName));
					if (search)
						pack.getEntity(search._id).then(bItem => resolve(bItem));
					else
						 resolve(false);
				}
				else
					resolve(false);
			})();			
		});
	});
	Promise.all(compSearches).then(comps => buffFound(comps.find(c => c)));
}
catch (err) {
	console.log(err, "Whatever you did didn't work");
}
function buffFound(buff) {
	if (typeof levelOverride != 'undefined') buff.data.data.level = levelOverride;
	if (typeof altName != 'undefined') buff.data.name = altName;
	targetActors.forEach(act => {
		if (act && act.hasPerm(game.user, 'OWNER')) {
			let presentBuff = act.items.find(o => {return o.data.type == 'buff' && (o.name == buffName || o.name == altName);});
			let updateArgs = [];
			switch(operator.toLowerCase()) {
				case 'apply':
					if (!buff.data.data.active)
						buff.data.data.active = true;
					
					if (presentBuff)
						presentBuff.update({'data.active': true, 'data.level': buff.data.data.level, 'name': buff.data.name});
					else
						act.createOwnedItem(buff);			
					break;
				case 'remove':
					if (presentBuff)
						presentBuff.delete();				
					break;
				case 'toggle':
					if (presentBuff)
						presentBuff.update({'data.active': !getProperty(presentBuff.data, 'data.active'), 'data.level': buff.data.data.level, 'name': buff.data.name});
					else {
						if (!buff.data.data.active)
							buff.data.data.active = true;
						
						act.createOwnedItem(buff);
					}
					break;
				case 'activate':
					if (presentBuff)
						presentBuff.update({'data.active': true, 'data.level': buff.data.data.level, 'name': buff.data.name});
					break;
				case 'deactivate':
					if (presentBuff)
						presentBuff.update({'data.active': false, 'data.level': buff.data.data.level, 'name': buff.data.name});
					else
						act.createOwnedItem(buff);
					break;
				case 'change':
					if (presentBuff && typeof levelOverride != 'undefined')
						presentBuff.update({'data.level': buff.data.data.level, 'name': buff.data.name});
				default:
					ui.notifications.warn("That's not how you use this. See documentation.");
			}
		}
		else if (act)
			excludedActors.push(act);
	});
	var successStr = 'No tokens affected.',
		affectedActors = targetActors.length - excludedActors.length;
	if (affectedActors != 0)
		successStr = `${buff.data.name} was changed on ${affectedActors} token${(affectedActors > 1 ? 's' : '')}.`;
	if (excludedActors.length > 0) {
		successStr += ' Requesting GM assistance for the rest.';
		var remainingModList = modList.filter((o,p) => o != 'to' && modList[p-1] != 'to'),
			excludedNames = excludedActors.map(o => (o.token ? o.token.name : o.name)).join(', '),
			gmButtonTitle = `${operator} ${buffName} to remaining ${remainingModList.join(' ')}`.trim(),
			enrichedButton = `<a class="entity-link" data-entity="Macro" data-id="${macroId}"><i class="fas fa-terminal"></i> ${gmButtonTitle}</a>`,
			flavorText = `I'm trying to<br>${enrichedButton}<br>The remaining are: ${excludedNames}`,
			remainingUUID = excludedActors.map(o => (o.token ? o.token.uuid : o.uuid)).join(','),
			spoofedRoll = new Roll(excludedActors.length.toString()).roll();
		spoofedRoll.formula = 'Will affect:';
			ChatMessage.create({
			blind: true,
			sound: null,
			flavor: flavorText,
			speaker: ChatMessage.getSpeaker(),
			content: 'I will be destroyed',
			type: CONST.CHAT_MESSAGE_TYPES.ROLL,
			roll: spoofedRoll,
			whisper: ChatMessage.getWhisperRecipients("GM"),
			flags: {applyBuff: {remaining: remainingUUID}}
		});
	}
	ui.notifications.info(successStr.trim());
}