var headers = ['/*-\t\t\tDOCUMENTATION\t\t\t-*/\n', '\n/*-\t\t\tCONFIGURATION\t\t\t-*/\n', '\n\n/*-\t\t\tCOMMAND\t\t\t\t\t-*/'];
game.macros.forEach(m => {
	if (m != this) {
		if (m.data.command?.indexOf('/*-') == -1) {
			let parts = m.data.command.split(/(const targetMacro = ".*";)/);
			if (parts.length == 3) {
				let newCommand = headers[0] + parts[0] + headers[1] + parts[1] + headers[2] + parts[2];
				m.update({command: newCommand});
			}
		}
	}
});