//Run this to apply updates. If you meet a minimum version requirement, you shouldn't lose any data.
//If you need to add your own code somewhere, create a header and it will be preserved
var updateList = ['applyBuff', 'useAction', 'setTarget'],
	corsAnywhere = 'https://cors-anywhere.herokuapp.com/https://gitlab.com/JusticeNoon/foundry-macros/-/raw/master/PF1E%20Entity-Link/',
	url = corsAnywhere + updateList[0] + '.js',
	splitReg = /\/\*-\s*(.*?)\s*-\*\//gm,
	headerList = ['DOCUMENTATION', 'CONFIGURATION', 'COMMAND'];

updateList.forEach(o =>{
	let url = corsAnywhere + o + '.js';
	fetch(url).then(response => {
		if (response.ok)
			return response.text();
		return Promise.reject(response);
	}).then(data => {
		var existing = game.macros.getName(o);
		if (!existing)
			return ui.notifications.warn('Not imported');
		else if (existing.data.command == data)
			return 'Already updated';
			
		var existSplits = existing.data.command.split(splitReg);
		if (existSplits.length == 1) {
			let c = Dialog.confirm({
				title: 'Overwrite ' + o + '?',
				content: `<p> ${o} is so out of date it doesn't support partial updates. Pressing yes will replace it completely. Any user modications to ${o} will be lost.</p>`,
				yes: () => existing.update({command: data}),
				no: () => console.log('It\'s okay. Change is hard'),
				defaultYes: false
			});
		}
		else {
			if (existSplits.length % 2 == 1 && existSplits[0] == '')
				existSplits = toMap(existSplits.slice(1))
			else
				throw ui.notifications.error('Malformed target macro');
			
			var dataSplits = data.split(splitReg);
			if (dataSplits.length % 2 == 1 && dataSplits[0] == '')
				dataSplits = toMap(dataSplits.slice(1));
			else
				throw ui.notifications.error('Malformed source macro. Please @me on discord immediately');
			
			let formData = '';
			headerList.forEach(head => {
				if (dataSplits.has(head) && dataSplits.get(head)?.trim() != existSplits.get(head)?.trim()) {
					formData += `<label>${head} --- </label>`;
					formData += `<input type="radio" name="${head}" value="exist" ${(head == 'CONFIGURATION' ? 'checked' : '')}><label>Original</label>`;
					formData += `<input type="radio" name="${head}" value="data" ${(head != 'CONFIGURATION' ? 'checked' : '')}><label>Updated</label><br>`;
				}
			});
			if (formData == '')
				return 'No changes found. Headers must be different';
			else {
				let d = new Dialog({
					title: o + ' has differences',
					content: 	'<p>The following areas are different and need adjudication. \n' +
								'Choose which to keep. Areas not listed here will not be changed.</p><form>' +
								formData + '</form>',
					buttons: {
						apply: {
							icon: '<i class="fas fa-check"></i>',
							label: "Apply",
							callback: html => {
								let changes = Object.fromEntries((new FormData(html[0].querySelector('form'))).entries());
								let remake = '';
								existSplits.forEach((text, head) => {
									if (!changes[head] || changes[head] == 'exist')
										remake += '\n/*-\t\t\t' + head + '\t\t\t-*/\n' + text;
									else
										remake += '\n/*-\t\t\t' + head + '\t\t\t-*/\n' + dataSplits.get(head);
								});
								existing.update({command: remake.trim()});
							}
						},
						cancel: {
							icon: '<i class="fas fa-times"></i>',
							label: "Cancel",
							callback: () => {console.log('You can\'t cancel me. I quit.')}
						}
					},
					default: "apply"
				});
				d.render(true);
			}
		}
	}).catch(error => {
		console.warn('Something went wrong.', error);
	});
});

var toMap = function(r) {
	let myMap = new Map();
	for (var k = 0; k < r.length; k += 2)
		myMap.set(r[k], r[k+1]);
	return myMap;
}