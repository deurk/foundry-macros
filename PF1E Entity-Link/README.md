Here's a tutorial and example. https://youtu.be/iaafXnT9nLM
Maybe I can put some examples here.

All examples below are assumed to be in the format `@Macro[macroName]{Example Text}`
- setTarget:
    - Clear Targets
        - I'd recommend putting this on your hotbar using a callMacro if you're going to use
        the targetting feature often. It is really annoying to manually deselect.
    - Select myself
        - If run through a Roll card, it will select the actor's tokens on the current scene.
        - For GMs it'll select the currently selected tokens.
        - For players, it'll select their character.