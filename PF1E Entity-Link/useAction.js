/*-			DOCUMENTATION			-*/
// Syntax: [<ActorOverride>:] <Action Name> [#<type>]
// ie @Macro[useAction]{Manyshot #attack}
// Action Name: 
//		All items on the character sheet will be searched.
//		The first one to be found that contains an action will be used.
// Type (Optional, Prefixed by "#"):
//		If not specified, it will not care and just return the first actionable action.
//		Capitilization does not matter here
// ActorOverride (Optional, Suffixed by ":"):
//		Sets the remaining actions to use the overriden actor by name
//		If not specified, will use the default target actor priority when searching (speaker, character, selected)
//		This is case sensitive. You can reset back to default by providing the keyword "My:"
//		ie @Macro[useAction]{His: Manyshot, AmmoCount, My: OP Dodge Ability}
// You may separate your actions by comma/semi-colon to execute multiple ie @Macro[useAction]{Magic Missile, Magic Missile, Crossbow}
// If it can't find an exact match for action, it will use the first actionable item containing the name
// Supports label mode. [["-El Diablo-, +5 Greatsword of Exploding",1]]@Macro[useAction]{::Time to hit something}

/*-			COMMAND					-*/
try {
	var inputText = window.macroChain?.pop() || event.srcElement.closest('button,a')?.textContent.trim(),
		chatMessage = game.messages.get(event.srcElement.closest('.message')?.getAttribute('data-message-id')),
		targetActor = canvas.tokens.objects.children.find(o => o.id == chatMessage?.data.speaker?.token)?.actor ?? 
			game.actors.get(chatMessage?.data.speaker?.actor) ?? game.user.character ?? canvas.tokens.controlled[0]?.actor,
		argParse = /^(?:(.*):\s*)?(.*?)(?: ?#(.*))?$/,
		actionList;
	if (inputText.indexOf('::') == 0)
		inputText = /['`"](.*)['`"],.*/.exec(event.srcElement.previousElementSibling?.title.trim())?.[1];
	if (inputText.indexOf(';') > -1)
		actionList = inputText.split(';').map(o => o.trim());
	else
		actionList = inputText.split(',').map(o => o.trim());
}
catch (err) {
	console.log(err, "Whatever you did didn't work");
}
actionList.forEach(act => {
	var [actorOverride, actionName, actionType] = act.match(argParse).slice(1);
	if (actorOverride) {
		if (actorOverride.toLowerCase() == 'my') targetActor = game.actors.get(chatMessage?.data.speaker?.actor) ?? game.user.character ?? canvas.tokens.controlled[0]?.actor;
		else targetActor = game.actors.getName(actorOverride);
	}
	if (actionType) actionType = actionType.toLowerCase();
	if (targetActor) {
		if (!targetActor.hasPerm(game.user, "OWNER")) return ui.notifications.warn(game.i18n.localize("PF1.ErrorNoActorPermission"));
		var backupAction = false,
			item = false;
		item = targetActor.items.find(o => {
			if (o.hasAction == false) return false;
			if (actionType && actionType != o.type) return false;
			if (!backupAction && o.name.toLowerCase().indexOf(actionName.toLowerCase()) > -1)
				backupAction = o;
			return o.name == actionName;
		});
		if (!item) {
			if (backupAction)
				item = backupAction;
			else 
				return ui.notifications.warn(`You don't have an action named ${actionName}`);
		}
		if (!game.keyboard.isDown("Control"))
			item.use({skipDialog: (game.settings.get("pf1", "skipActionDialogs") !== game.keyboard.isDown("Shift"))});
		else
			item.roll();
	}
});